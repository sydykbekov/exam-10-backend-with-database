const express = require('express');
const news = require('./app/news');
const app = express();
const cors = require('cors');
const mysql = require('mysql');
const comments = require('./app/comments');

const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'root',
    database : 'exam'
});

connection.connect((err) => {
    if (err) throw err;

    app.use('/news', news(connection));

    app.use('/comments', comments(connection));

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});
