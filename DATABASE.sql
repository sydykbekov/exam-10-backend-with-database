CREATE SCHEMA `exam` ;
USE `exam`;

CREATE TABLE `exam`.`news` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `title` VARCHAR(255) NOT NULL,
    `content` TEXT NOT NULL,
    `image` VARCHAR(255),
    `date` VARCHAR(255) NOT NULL
);

CREATE TABLE `exam`.`comments` (
	`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `news_id` INT NOT NULL,
    `author` VARCHAR(255),
    `comment` TEXT NOT NULL,
    INDEX `FK_news_idx` (`news_id`),
		FOREIGN KEY (`news_id`)
        REFERENCES `news` (`id`)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);

SELECT * FROM `news`;
