const express = require('express');
const router = express.Router();

const createRouter = (db) => {
    router.get('/', (req, res) => {
        if (req.query.news_id) {
            db.query('SELECT * FROM `comments` WHERE news_id=' + req.query.news_id + ';', function (error, results) {
                if (error) throw error;

                res.send(results);
            });
        } else {
            db.query('SELECT * FROM `comments`', function (error, results) {
                if (error) throw error;

                res.send(results);
            });
        }
    });

    router.post('/', (req, res) => {
        const comment = req.body;

        if (comment.author === '') {
            comment.author = 'Anonymous';
        }

        db.query(
            'INSERT INTO `comments` (`news_id`, `author`, `comment`) ' +
            'VALUES (?, ?, ?)',
            [comment.id, comment.author, comment.comment],
            (error, results) => {
                if (error) throw error;

                res.send(results);
            }
        );
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `comments` WHERE id=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;