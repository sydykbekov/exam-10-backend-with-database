const express = require('express');
const router = express.Router();
const multer = require('multer');
const nanoid = require('nanoid');
const path = require('path');

const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = (db) => {
    router.get('/', (req, res) => {
        db.query('SELECT `id`, `title`, `image`, `date` FROM `news`', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.post('/', upload.single('image'), (req, res) => {
        const news = req.body;

        if (req.file) {
            news.image = req.file.filename;
        } else {
            news.image = null;
        }

        db.query(
            'INSERT INTO `news` (`title`, `content`, `image`, `date`) ' +
            'VALUES (?, ?, ?, ?)',
            [news.title, news.content, news.image, new Date().toISOString()],
            (error, results) => {
                if (error) throw error;

                news.id = results.insertId;
                res.send(news);
            }
        );
    });

    router.get('/:id', (req, res) => {
        db.query('SELECT * FROM `news` WHERE id=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    router.delete('/:id', (req, res) => {
        db.query('DELETE FROM `news` WHERE id=' + req.params.id + ';', function (error, results) {
            if (error) throw error;

            res.send(results);
        });
    });

    return router;
};

module.exports = createRouter;